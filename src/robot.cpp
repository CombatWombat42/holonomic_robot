/**
 * @brief Run toe robot app
 *
 * @param argc number of arguments
 * @param argv pointer to arguments
 * @return int
 */

/**
 * I'm getting a defenition of struct sched_param from both
 * 		/usr/arm-linux-gnueabihf/include/bits/sched.h:74 (included by
 * C++ headers string and memory)
 * 		/usr/arm-linux-gnueabihf/include/linux/sched/types.h:7 (included
 * by me to get struct sched_attr) Defining _BITS_TYPES_STRUCT_SCHED_PARAM will
 * allow me to elemenate one
 */
#define _BITS_TYPES_STRUCT_SCHED_PARAM 1

#include <getopt.h>

#include <iostream>

#include "motion_state.hpp"
#include "robot_configuration.hpp"
#include "servo_stm32.hpp"

/**
 * @brief a struct of any paramaters to the program that have been parsed by
 * getopt to return to main
 *
 * @param config_file the path to the config file to load
 *
 */
typedef struct config_opts {
  std::string config_file;
} config_opts_t;

static void print_usage(const char* prog) {
  printf("Usage: %s [-c]\n", prog);
  puts(
      "  -c --config_file path to the config file to read from, defaults to "
      "config.yaml\n");
  exit(1);
}

static config_opts_t parse_opts(int argc, char* argv[]) {
  config_opts_t ret_opts;
  ret_opts.config_file.assign("config.yaml");
  while (1) {
    static const struct option lopts[] = {
        {"config_file", required_argument, nullptr, 'c'},
        {NULL, 0, 0, 0},
    };
    int c;

    c = getopt_long(argc, argv, "c:", lopts, NULL);

    if (c == -1) { return ret_opts; }

    switch (c) {
      case 'c':
        ret_opts.config_file.assign(optarg);
        break;
      default:
        print_usage(argv[0]);
        break;
    }
  }
}

int main(int argc, char* argv[]) {
  printf("# %s compilied on %s:%s\n", __FILE__, __DATE__, __TIME__);
  auto options = parse_opts(argc, argv);

  printf("Starting robot\n");

  int8_t xSpeed, ySpeed, zSpeed;
  int8_t speedAmount = 5;
  char input;

  auto robotConfiguraiton = robotConfiguration(options.config_file);

  auto motionState =
      std::make_unique<MotionState>(robotConfiguraiton.config["motor"]);

  while (1) {
    std::cin >> input;
    switch (input) {
      case 'w':
        if ((xSpeed + speedAmount) > SCHAR_MAX) {
          printf("Cannot increase speed (%d) by %d to beyond %d\n",
                 xSpeed,
                 speedAmount,
                 SCHAR_MAX);
        } else {
          xSpeed += speedAmount;
        }
        break;
      case 's':
        if ((xSpeed - speedAmount) < SCHAR_MIN) {
          printf("Cannot decrease speed (%d) by %d to beyond %d\n",
                 xSpeed,
                 speedAmount,
                 SCHAR_MIN);
        } else {
          xSpeed -= speedAmount;
        }
        break;
      case 'a':
        if ((ySpeed + speedAmount) > SCHAR_MAX) {
          printf("Cannot increase speed (%d) by %d to beyond %d\n",
                 ySpeed,
                 speedAmount,
                 SCHAR_MAX);
        } else {
          ySpeed += speedAmount;
        }
        break;
      case 'd':
        if ((ySpeed - speedAmount) < SCHAR_MIN) {
          printf("Cannot decrease speed (%d) by %d to beyond %d\n",
                 ySpeed,
                 speedAmount,
                 SCHAR_MIN);
        } else {
          ySpeed -= speedAmount;
        }
        break;
      case 'q':
        if ((zSpeed + speedAmount) > SCHAR_MAX) {
          printf("Cannot increase speed (%d) by %d to beyond %d\n",
                 zSpeed,
                 speedAmount,
                 SCHAR_MAX);
        } else {
          zSpeed += speedAmount;
        }
        break;
      case 'e':
        if ((zSpeed - speedAmount) < SCHAR_MIN) {
          printf("Cannot decrease speed (%d) by %d to beyond %d\n",
                 zSpeed,
                 speedAmount,
                 SCHAR_MIN);
        } else {
          zSpeed -= speedAmount;
        }
        break;
      case 'r':
        printf("Stopping\n");
        xSpeed = 0;
        ySpeed = 0;
        zSpeed = 0;
        break;
      case 'z':
        exit(0);
    }
    motionState->set_speed(xSpeed, ySpeed, zSpeed);
  }
}