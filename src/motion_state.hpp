/**
 * @file motion_state.hpp
 * @author Sam Povilus (robot@povil.us)
 * @brief take in steering commands and output motor position commands
 * @version 0.1
 * @date 2020-07-18
 *
 * @copyright Copyright (c) 2020
 *
 */

#ifndef MOTION_STATE_H_
#define MOTION_STATE_H_
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/systems/angle/revolutions.hpp>
#include <boost/units/systems/si.hpp>
#include <climits>
#include <vector>

#include "servo_stm32.hpp"
#include "yaml-cpp/yaml.h"

class MotionState {
 private:
  ServoSTM32 servo;

 public:
  /**
   * @brief Construct a new Motion State object
   *
   * Initalize motor
   *
   * @param motion_control_node
   */
  MotionState(YAML::Node motion_control_node)
      : servo(motion_control_node["spi_file"].as<std::string>()){};

  static bool abs_compare(int a, int b) {
    return (std::abs(a) < std::abs(b));
  }

  void printvec(std::vector<float> const &input) {
    for (auto const &i : input) { std::cout << i << " "; }
    std::cout << std::endl;
  }

  void set_speed(float X, float Y, float spin) {
    std::vector<float> motorValues;
    motorValues.push_back((
        ((-1.0 * (sin(M_PI / 3.0) * X)) + (cos(M_PI / 3.0) * Y) + spin) / 3.0));
    motorValues.push_back(((-Y + spin) / 3.0));
    motorValues.push_back(
        (((sin(M_PI / 3.0) * X) + (cos(M_PI / 3.0) * Y) + spin) / 3.0));
    printf(
        "transformed X: %f Y: %f rot: %f into m0: %05.05f m1: "
        "%05.05f m2: %05.05f\n",
        X,
        Y,
        spin,
        motorValues.at(0),
        motorValues.at(1),
        motorValues.at(2));

    printvec(motorValues);

    /* Need to normalize and scale*/
    auto biggest = std::max_element(
        std::begin(motorValues), std::end(motorValues), abs_compare);
    std::transform(
        motorValues.begin(),
        motorValues.end(),
        motorValues.begin(),
        std::bind(std::multiplies<float>(), std::placeholders::_1, *biggest));
    printvec(motorValues);
    std::transform(
        motorValues.begin(),
        motorValues.end(),
        motorValues.begin(),
        std::bind(std::multiplies<float>(), std::placeholders::_1, 127));
    printvec(motorValues);

    servo.set_speed(motorValues.at(0), motorValues.at(1), motorValues.at(2));
  }
};
#endif  // MOTION_STATE_H