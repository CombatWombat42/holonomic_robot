/**
 * @file motor.hpp
 * @author Sam Povilus (robot@povil.us)
 * @brief An interface class spawning a thread that controlls a single motor
 * @version 0.1
 * @date 2020-07-11
 *
 * @copyright Copyright (c) 2020
 *
 * This class will provide a thread that controlls a motor the inital version is
 * targetd at a stepper
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include <linux/sched/types.h>

#include <chrono>
#include <ctime>
#include <exception>
#include <iostream>
#include <thread>

#include "schedule_wrappers.hpp"

class Motor {
 private:
  /**
   * @brief This is the main loop the motor performs
   *
   * This method  should not return, but it shold call sched_yield when it is
   * done with this loops tasks. See presentations on SCHED_DEADLINE for
   * details. https://youtu.be/TDR-rgWopgM?t=1124
   * https://elinux.org/images/f/fe/Using_SCHED_DEADLINE.pdf page 34
   *
   */
  virtual void run(void) = 0;
  /**
   * @brief Setup the thread to be scheduled by the deadline scheduler and run
   * the run method
   *
   * @param control_rate a timespec defining how often the controller needs to
   * run
   * @param runtime a timespec defining worst case runtime of run method
   */
  void start_thread(timespec control_rate, timespec runtime) {
    int ret;
    struct sched_attr attr;
    ret = ScheduleWrapper::sched_getattr(0, &attr, sizeof(attr), 0);
    if (ret < 0) {
      perror("Unable to get scheduler attributes");
      throw scheduler_exception;
    }
    attr.sched_policy = SCHED_DEADLINE;
    attr.sched_runtime = (runtime.tv_nsec +
                          (std::chrono::duration_cast<std::chrono::nanoseconds>(
                               std::chrono::seconds(runtime.tv_sec))
                               .count()));
    attr.sched_deadline = control_rate.tv_nsec +
                          (std::chrono::duration_cast<std::chrono::nanoseconds>(
                               std::chrono::seconds(control_rate.tv_sec)))
                              .count();
    ret = ScheduleWrapper::sched_setattr(0, &attr, 0);
    if (ret < 0) {
      perror(
          "Motor thread initilization failed, unable to set schedule"
          "attributes");
      throw scheduler_exception;
    }
    run();
    throw control_returned_exception;
  }

 protected:
  /**
   * @brief Construct a new Motor object
   *
   * Anything the run method needs to run will have to be passed in as part of
   * the derived class
   *
   * @param control_rate a timespec defining how often the controller needs to
   * run
   * @param runtime a timespec defining worst case runtime of run method
   */
  Motor(timespec control_rate, timespec runtime) {
    auto motorThread =
        std::thread(&Motor::start_thread, this, control_rate, runtime);
    motorThread.detach();
  }

  /**
   * @brief Set the desired relative position of the motor
   *
   * The motor will move to this relative position as best it can and hold there
   * unless this position is updated. This can be update mid move if a new
   * position is desired.
   *
   * @param desired_relative_position
   */
  virtual void set_desired_relative_position(
      boost::units::quantity<boost::units::si::plane_angle, double>) = 0;

 public:
  /**
   * @brief An exception to throw when the run method returns unexpectedly
   *
   */
  class ControlReturnedException : public std::exception {
    /**
     * @brief Let the user know they should not have returned from "run"
     *
     * @return const char* the string telling the user they are dumb
     */
    virtual const char* what() const throw() {
      return "Returned from a function that was expected to run forever";
    }
  } control_returned_exception;

  class SchedulerException : public std::exception {
   public:
    /**
     * @brief Something is happening with the sceduler
     *
     * @return const char* the string telling the user what happened
     */
    virtual const char* what() const throw() {
      return "Scheduler failed";
    }
  } scheduler_exception;
};
#endif  // MOTOR_H_