# Overview

This project is a simple interface to the stm32 stepper servo controller I am making.

It is initally intended to just tell the servo controller motor speeds over spi.


## SPI

I intend to use the SPI interface, I have played with the userspace SPI inerface (here)[https://bitbucket.org/CombatWombat42/sched_deadline_testing/src/master/src/sensor.hpp] and will likely adapt that class to read the accelerometer and gyro.


## Configuration

### YAML

using yaml-cpp, you need ot do the cmake command with '-DCMAKE_TOOLCHAIN_FILE=../arm.cmake'

# TODO

