#!/usr/bin/env bash

mkdir /opt/robot/
mkdir /opt/robot/sensors
mkdir /opt/robot/motors/

ln -s /dev/spidev1.1 /opt/robot/sensors/accelerometer_spi
ln -s /dev/spidev1.0 /opt/robot/sensors/gyro_spi

ln -s /sys/class/pwm/pwm-4:0/ /opt/robot/motors/motor0
ln -s /sys/class/pwm/pwm-1:0/ /opt/robot/motors/motor1
ln -s /sys/class/pwm/pwm-7:1/ /opt/robot/motors/motor2

ln -s /sys/class/gpio/gpio60 /opt/robot/motors/dir0
ln -s /sys/class/gpio/gpio4 /opt/robot/motors/dir1
ln -s /sys/class/gpio/gpio45 /opt/robot/motors/dir2
